<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_FreeGifts
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\FreeGifts\Api;

/**
 * Interface ProductGiftInterface
 * @package Mageplaza\FreeGifts\Api
 */
interface ProductGiftInterface
{
    /**
     * @param string $sku
     * @return array
     */
    public function getGiftsByProductSku($sku);
    
    /**
     * @param string $itemId
     * @return array
     */
    public function getGiftsByQuoteItemId($itemId);
    
    /**
     * @param string $quoteId
     * @param string $itemId
     * @return array
     */
    public function deleteGiftByQuoteItemId($quoteId, $itemId);
    
    /**
     * @param string $quoteId
     * @param string $ruleId
     * @param string $giftId
     * @return array
     */
    public function addGiftById($quoteId, $ruleId, $giftId);
}
