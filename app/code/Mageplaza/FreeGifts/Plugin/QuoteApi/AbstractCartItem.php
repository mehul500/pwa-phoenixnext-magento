<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_FreeGifts
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\FreeGifts\Plugin\QuoteApi;

use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote\ItemFactory;
use Mageplaza\FreeGifts\Helper\Rule as HelperRule;

/**
 * Class AbstractCartItem
 * @package Mageplaza\FreeGifts\Plugin\QuoteApi
 */
abstract class AbstractCartItem
{
    /**
     * @var HelperRule
     */
    protected $_helperRule;

    /**
     * @var ItemFactory
     */
    protected $_itemFactory;

    /**
     * GuestCartItem constructor.
     *
     * @param HelperRule $helperRule
     * @param ItemFactory $itemFactory
     */
    public function __construct(
        HelperRule $helperRule,
        ItemFactory $itemFactory
    ) {
        $this->_helperRule  = $helperRule;
        $this->_itemFactory = $itemFactory;
    }

    /**
     * @param Item $quoteItem
     *
     * @return array
     */
    public function getFreeGiftData($quoteItem)
    {
        if ($ruleId = (int) $quoteItem->getData(HelperRule::QUOTE_RULE_ID)) {
            return [
                'mp_free_gifts'         => true,
                'mp_free_gifts_message' => $this->_helperRule->getRuleById($ruleId)->getNoticeContent(),
            ];
        }

        return [];
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_helperRule->getHelperData()->isEnabled();
    }
}
