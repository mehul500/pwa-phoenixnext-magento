<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_FreeGifts
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\FreeGifts\Model\Api;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as TypeConfigurable;
use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\ItemFactory as QuoteItemFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Mageplaza\FreeGifts\Api\ProductGiftInterface;
use Mageplaza\FreeGifts\Helper\Rule as HelperRule;
use Mageplaza\FreeGifts\Model\Source\Apply;
use Mageplaza\FreeGifts\Observer\ScreenFreeGift;

/**
 * Class ProductGift
 * @package Mageplaza\FreeGifts\Model\Api
 */
class ProductGift implements ProductGiftInterface
{
    /**
     * @var HelperRule
     */
    protected $_helperRule;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var QuoteItemFactory
     */
    protected $_quoteItemFactory;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $_quoteIdMask;

    /**
     * @var QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * @var ScreenFreeGift
     */
    protected $_screenFreeGift;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * ProductGift constructor.
     *
     * @param HelperRule $helperRule
     * @param ProductRepositoryInterface $productRepository
     * @param QuoteItemFactory $quoteItemFactory
     * @param QuoteIdMaskFactory $quoteIdMask
     * @param QuoteFactory $quoteFactory
     * @param ScreenFreeGift $screenFreeGift
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        HelperRule $helperRule,
        ProductRepositoryInterface $productRepository,
        QuoteItemFactory $quoteItemFactory,
        QuoteIdMaskFactory $quoteIdMask,
        QuoteFactory $quoteFactory,
        ScreenFreeGift $screenFreeGift,
        CheckoutSession $checkoutSession
    ) {
        $this->_helperRule        = $helperRule;
        $this->_productRepository = $productRepository;
        $this->_quoteItemFactory  = $quoteItemFactory;
        $this->_quoteIdMask       = $quoteIdMask;
        $this->_quoteFactory      = $quoteFactory;
        $this->_screenFreeGift    = $screenFreeGift;
        $this->_checkoutSession   = $checkoutSession;
    }

    /**
     * @inheritDoc
     */
    public function getGiftsByProductSku($sku)
    {
        try {
            $product  = $this->_productRepository->get($sku);
            $response = $this->_helperRule->setApply(Apply::ITEM)->setProduct($product)->getValidatedRules();
        } catch (Exception $e) {
            $response = $this->getErrorResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * @inheritDoc
     */
    public function getGiftsByQuoteItemId($itemId)
    {
        $itemModel = $this->_quoteItemFactory->create();
        $item      = $itemModel->load($itemId);
        $quote     = $this->getQuoteById($item->getQuoteId());
        $response  = $this->getErrorResponse(__('This quote item does not exist.'));

        try {
            if ($item->getId()) {
                $product  = $this->_productRepository->getById($item->getDataByKey('product_id'));
                $response = $this->_helperRule->setQuote($quote)
                    ->setApply(Apply::ITEM)
                    ->setProduct($product)
                    ->getValidatedRules();
            }
        } catch (Exception $e) {
            $response = $this->getErrorResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * @inheritDoc
     */
    public function deleteGiftByQuoteItemId($quoteId, $itemId)
    {
        $quote = $this->getQuoteById($quoteId);
        if (!$quote) {
            return $this->getErrorResponse(__('This quote does not exist.'));
        }
        $item = $quote->getItemById($itemId);
        if (!$item) {
            return $this->getErrorResponse(__('Current quote does not contain this item.'));
        }

        $response = $this->getErrorResponse(__('This item is not a free gift.'));
        if ((int) $item->getDataByKey(HelperRule::QUOTE_RULE_ID)) {
            $quote->removeItem($item->getId());
            $response = [
                [
                    'success' => true,
                ]
            ];
        }

        try {
            $quote->save();
            $quote->setTotalsCollectedFlag(false);
        } catch (Exception $e) {
            $response = $this->getErrorResponse($e->getMessage());
        }

        return $response;
    }

    /**
     * @inheritDoc
     */
    public function addGiftById($quoteId, $ruleId, $giftId)
    {
        $helperGift = $this->_helperRule->getHelperGift();
        $quote      = $this->getQuoteById($quoteId);
        if (!$quote) {
            return $this->getErrorResponse(__('This quote does not exist.'));
        }
        $rule = $this->_helperRule->getRuleById($ruleId);
        if (!$rule->getId()) {
            return $this->getErrorResponse(__('This rule does not exist.'));
        }

        try {
            /** @var Product $gift */
            $gift = $this->_productRepository->getById($giftId);
            if (!$helperGift->isGiftInStock($giftId)) {
                return $this->getErrorResponse(__('This gift is currently out of stock.'));
            }
            $validRules = $this->_helperRule->setExtraData(false)->setQuote($quote)->getAllValidRules();
        } catch (Exception $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        $validRuleIds = array_column($validRules, 'rule_id');
        $ruleGiftIds  = array_keys($rule->getGiftArray());
        if (!in_array((int) $giftId, $ruleGiftIds, true)) {
            return $this->getErrorResponse(__('This gift does not belong to the current rule.'));
        }
        if (!in_array($ruleId, $validRuleIds, true)) {
            return $this->getErrorResponse(__('This rule is invalid for the current quote.'));
        }
        if ($helperGift->isMaxGift($ruleId)) {
            return $this->getErrorResponse(__('Maximum number of gifts added.'));
        }
        if ((int) $gift->getRequiredOptions() || is_array($helperGift->requireLinks($gift))) {
            return $this->getErrorResponse(__('This gift product requires custom options.'));
        }
        if ($helperGift->isGiftAdded($giftId, $quoteId)) {
            return $this->getErrorResponse(__('This gift is already added.'));
        }

        $giftParams = ['product' => $giftId, 'qty' => 1, HelperRule::OPTION_RULE_ID => $ruleId];
        $gift->addCustomOption(HelperRule::QUOTE_RULE_ID, $ruleId);
        if ($gift->getTypeId() === TypeConfigurable::TYPE_CODE) {
            $giftParams['super_attribute'] = $this->_screenFreeGift->getSuperAttributes($gift);
        }

        try {
            $quote->addProduct($gift, new DataObject($giftParams));
            $quote->save();
            $quote->setTotalsCollectedFlag(false);
        } catch (Exception $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        $deletedGifts = $this->_checkoutSession->getFreeGiftsDeleted();
        if (isset($deletedGifts[(int) $ruleId])) {
            $deletedGifts[(int) $ruleId] = $this->removeDeletedGift($deletedGifts[(int) $ruleId], (int) $giftId);
        }
        $this->_checkoutSession->setFreeGiftsDeleted($deletedGifts);

        return [
            [
                'success' => true,
            ]
        ];
    }

    /**
     * @param $quoteId
     *
     * @return Quote|false
     */
    public function getQuoteById($quoteId)
    {
        $quoteFactory     = $this->_quoteFactory->create();
        $quoteMaskFactory = $this->_quoteIdMask->create();
        if ($quoteMaskId = $quoteMaskFactory->load($quoteId, 'masked_id')->getDataByKey('quote_id')) {
            $quoteId = $quoteMaskId;
        }
        $quote = $quoteFactory->load($quoteId);

        return $quote->getId() ? $quote : false;
    }

    /**
     * @param $message
     *
     * @return array
     */
    public function getErrorResponse($message)
    {
        return [
            [
                'error'   => true,
                'message' => $message
            ]
        ];
    }

    /**
     * @param array $gifts
     * @param int $giftId
     *
     * @return mixed
     */
    public function removeDeletedGift($gifts, $giftId)
    {
        foreach ($gifts as $index => $deleteGift) {
            if ($giftId === (int) $deleteGift) {
                unset($gifts[$index]);
            }
        }

        return $gifts;
    }
}
