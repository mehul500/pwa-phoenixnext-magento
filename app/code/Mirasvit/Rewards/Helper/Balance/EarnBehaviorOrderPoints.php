<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rewards\Helper\Balance;


class EarnBehaviorOrderPoints
{
    private $rewardsBehavior;
    private $rewardsReferral;
    private $storeFactory;

    public function __construct(
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Mirasvit\Rewards\Helper\BehaviorRule $rewardsBehavior,
        \Mirasvit\Rewards\Helper\Referral $rewardsReferral
    ) {
        $this->storeFactory    = $storeFactory;
        $this->rewardsBehavior = $rewardsBehavior;
        $this->rewardsReferral = $rewardsReferral;
    }

    /**
     * Behavior "create order" event
     * @param \Magento\Sales\Model\Order|\Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return void
     */
    public function earnBehaviorOrderPoints($order)
    {
        if ($order->getCustomerId()) {
            $this->rewardsBehavior->processRule(
                \Mirasvit\Rewards\Model\Config::BEHAVIOR_TRIGGER_CUSTOMER_ORDER,
                $order->getCustomerId(),
                $this->storeFactory->create()->load($order->getStoreId())->getWebsiteId(),
                $order->getId(),
                ['order' => $order]
            );
        }
        $this->rewardsReferral->processReferralOrder($order);
    }

}