<?php
/*
 * Created by 2C2P
 * Date 20 June 2017
 * This Response action method is responsible for handle the 2c2p payment gateway response.
 */

namespace P2c2p\P2c2pPayment\Controller\Payment;

use Psr\Log\LoggerInterface;

class Response extends \P2c2p\P2c2pPayment\Controller\AbstractCheckoutApiAction
{
	public function execute()
	{
		/*$post = $this->getRequest()->getPostValue();

		//If payment getway response is empty then redirect to home page directory.
		if(empty($post) || empty($post['order_id'])){
			$this->_redirect('');
			return;
		}*/
		if(!array_key_exists('paymentResponse',$_REQUEST)){
			$this->_redirect('');
			return;
		}
		$result = $_REQUEST["paymentResponse"]; 
		$response = $this->parse123Response($result);

		$xmlResponse=simplexml_load_string($response);
		$post = array();

	    $rc_date = date('Y-m-d h:i:s');
	    $reponseTime = time();
	    $reponsefilename = '/var/www/html/2c2p/frontend_'.$reponseTime.'.txt';

		foreach ($xmlResponse as $index => $node)
	    {
	        $post[$index] = (String)$node;
			$loggerMessage = $rc_date." : 2c2p Receiving [" . $index . "][".(String)$node."].";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
	    }
		//If payment getway response is empty then redirect to home page directory.
		if(empty($post) || empty($post['uniqueTransactionCode'])){
			$this->_redirect('');
			return;
		}

		$post = $this->parse2c2pResponse($post);
		$hashHelper   = $this->getHashHelper();
		$configHelper = $this->getConfigSettings();
		$objCustomerData = $this->getCustomerSession();
		//$isValidHash  = $hashHelper->isValidHashValue($post,$configHelper['secretKey']);
		/*if ($configHelper['mode']) {
				$secretKey = "7jYcp4FxFdf0";	//Get SecretKey from 2C2P PGW Dashboard
		} else {
				$secretKey = $configHelper['secretKey'];
		}*/
		$secretKey = $configHelper['secretKey'];
		$isValidHash  = $hashHelper->isValidHashValue2($post,$secretKey);
		//Get Payment getway response to variable.
		$payment_status_code = $post['channel_response_code'];
		$transaction_ref 	 = $post['transaction_ref'];
		$approval_code   	 = $post['approval_code'];
		$payment_status  	 = $post['payment_status'];
		$order_id 		 	 = $post['order_id'];

		//Get the object of current order.
		$order = $this->getOrderDetailByOrderId($order_id);

		//If order is empty then redirect to home page. Because order is not avaialbe.
		if(empty($order)) {
			$this->_redirect('');
			return;
			//return;
		}

		//Check whether hash value is valid or not If not valid then redirect to home page when hash value is wrong.
		if(!$isValidHash) 
		{
			if ($order->getId()){
			    $order->setState(\Magento\Sales\Model\Order::STATUS_FRAUD);
				$order->setStatus(\Magento\Sales\Model\Order::STATUS_FRAUD);
				$order->save();
			}
			
			$loggerMessage = $rc_date." : Can't receive data from 2c2p&123 Service.";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

			$this->_redirect('');
			return;
			//return;
		}

		$metaDataHelper = $this->getMetaDataHelper();
		$metaDataHelper->savePaymentGetawayResponse($post,$order->getCustomerId());

		//check payment status according to payment response.
		if(strcasecmp($payment_status_code, "00") == 0) {
			//IF payment status code is success

			if(!empty($order->getCustomerId()) && !empty($post['stored_card_unique_id'])) {
				$intCustomerId = $order->getCustomerId();
				$boolIsFound = false;

				// Fetch data from database by using the customer ID.
				$objTokenData = $metaDataHelper->getUserToken($intCustomerId);

				$arrayTokenData = array('user_id' => $intCustomerId,
					'stored_card_unique_id' => $post['stored_card_unique_id'],
					'masked_pan' => $post['masked_pan'],
					'created_time' =>  date("Y-m-d H:i:s"));

				/*
				   Iterate foreach and check whether token key is present into p2c2p_token table or not.
				   If token key is already present into database then prevent insert entry otherwise insert token entry into database.
				*/
				foreach ($objTokenData as $key => $value) {
					if(strcasecmp($value->getData('masked_pan'), $post['masked_pan']) == 0 &&
					   strcasecmp($value->getData('stored_card_unique_id'), $post['stored_card_unique_id']) == 0) {
						$boolIsFound = true;
						break;
					}
				}

				if(!$boolIsFound) {
					$metaDataHelper->saveUserToken($arrayTokenData);
				}
			}

			//Set the complete status when payment is completed.
			/*if ($order->getId()){
				$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
				$order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
				$order->save();
			}*/

			
			$orderIds = intval($order_id);
			$this->_eventManager->dispatch('checkout_onepage_controller_success_action',['order_ids' => [$orderIds]]);
			$loggerMessage = $rc_date." : Set the complete status when payment is completed.";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			

			$this->executeSuccessAction($post);
			return;

		}
		else if(strcasecmp($payment_status_code, "001") == 0) {
			//Set the Pending payment status when payment is pending. like 123 payment type.
			if ($order->getId()){
				$oldStatus = $order->getStatus();
				if($oldStatus == "Packing" || $oldStatus == "Shipped" || $oldStatus == "Complete" || $oldStatus == "Processing" || $oldStatus == "Order_Received")
					return;
				$order->setState("Pending_2C2P");
				$order->setStatus("Pending_2C2P");
				$order->save();
			}
			
			$orderIds = intval($order_id);
			$loggerMessage = $rc_date." : Set the Pending payment status when payment is pending. ";
 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			$this->_eventManager->dispatch('checkout_onepage_controller_success_action',['order_ids' => [$orderIds]]);
			$this->executeSuccessAction($post);
			return;
		}
		else if(strcasecmp($payment_status_code, "000") != 0) {
			//If payment status code is cancel/Error/other.
			$loggerMessage = $rc_date." : payment status code is cancel/Error/other.";

			if ($order->getId()){
				$oldStatus = $order->getStatus();
				if($oldStatus == "Packing" || $oldStatus == "Shipped" || $oldStatus == "Complete" || $oldStatus == "Processing" || $oldStatus == "Order_Received")
					return;
				$order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
				$order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
				$order->save();
			}

 			file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
			$this->executeCancelAction();
			return;
		}
	}
}
