<?php
namespace TBA\TBABase\Setup;
use \Psr\Log\LoggerInterface;
 
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Model\Order;
 
class InstallData implements InstallDataInterface
{
    /**
     * @var \Magento\Sales\Setup\SalesSetupFactory
     */
    protected $salesSetupFactory;
    protected $logger;
 
    /**
     * @param \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
     */
    public function __construct(LoggerInterface $logger,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
    ) {
        $this->logger = $logger;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->logger->info('TBABase InstallData __construct');
    }
 
    /**
     * {@inheritDoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) 
    {
        $this->logger->info('TBABase InstallData Start');
        $this->installLOBOT($setup,$context);
        $this->installPreOrder($setup,$context);
        $this->installLOBOTShippingDate($setup,$context);
    }

    public function installLOBOT(ModuleDataSetupInterface $setup, ModuleContextInterface $context) 
    {
        $this->logger->info('LOBOT InstallData Start');
        try 
        {
            $installer = $setup;
 
            $installer->startSetup();
     
            $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $installer]);
     
            $salesSetup->addAttribute(Order::ENTITY, 'trackingNumber', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'=> 255,
                'visible' => true,
                'nullable' => true
            ]);
            $this->logger->info('LOBOT InstallData');
     
            $installer->endSetup();
        } 
        catch (Exception $e) 
        {
            $this->logger->info('LOBOT InstallData Wrong :: ' . $e->__toString());
        }
    }

    public function installLOBOTShippingDate(ModuleDataSetupInterface $setup, ModuleContextInterface $context) 
    {
        $this->logger->info('LOBOT InstallData Start');
        try 
        {
            $installer = $setup;
 
            $installer->startSetup();
     
            $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $installer]);
     
            $salesSetup->addAttribute(Order::ENTITY, 'shippingDate', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length'=> 255,
                'visible' => true,
                'nullable' => true
            ]);
            $this->logger->info('LOBOT InstallData');
     
            $installer->endSetup();
        } 
        catch (Exception $e) 
        {
            $this->logger->info('LOBOT InstallData Wrong :: ' . $e->__toString());
        }
    }

    public function installPreOrder(ModuleDataSetupInterface $setup, ModuleContextInterface $context) 
    {
        $this->logger->info('TBA_PreOrder InstallData Start');
        try 
        {
            $installer = $setup;

            $installer->startSetup();
            $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $installer]);
     
            $salesSetup->addAttribute(Order::ENTITY, 'is_delivery_on_ready', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                'default'=> false,
                'visible' => true,
                'nullable' => false
            ]);
            $this->logger->info('TBA_PreOrder InstallData');
           

            $installer->endSetup();
        } 
        catch (Exception $e) 
        {
            $this->logger->info('TBA_PreOrder InstallData Wrong :: ' . $e->__toString());
        }
    }
}