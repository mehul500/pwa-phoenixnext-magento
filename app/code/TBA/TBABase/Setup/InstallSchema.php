<?php
namespace TBA\TBABase\Setup;
use \Psr\Log\LoggerInterface;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface
{
    protected $salesSetupFactory;
    protected $logger;
 
    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
        $this->logger->info('TBABase InstallSchema __construct');
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->logger->info('TBABase InstallSchema Start');
        $this->installLOBOT($setup,$context);
        $this->installPreOrder($setup,$context);
    }

    public function installLOBOT(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->logger->info('LOBOT InstallSchema Start');
        try 
        {
            $installer = $setup;

            /**
             * Prepare database for install
             */
            $installer->startSetup();

            /*try {
                  // Required tables
                  $statusTable = $installer->getTable('sales_order_status');
                  $statusStateTable = $installer->getTable('sales_order_status_state');

                  // Insert statuses
                  $installer->getConnection()->insertArray(
                    $statusTable,
                    array('status','label'),
                    array(array('status' => 'Packing', 'label' => 'Packing') , array('status' => 'Shipped', 'label' => 'Shipped'))
                    );

                  // Insert states and mapping of statuses to states
                  $installer->getConnection()->insertArray(
                    $statusStateTable,
                    array(
                      'status',
                      'state',
                      'is_default',
                      'visible_on_front'
                      ),
                    array(
                      array(
                        'status' => 'Packing',
                        'state' => 'Packing',
                        'is_default' => 0,
                        'visible_on_front' => 1
                        ),
                      array(
                        'status' => 'Shipped',
                        'state' => 'Shipped',
                        'is_default' => 0,
                        'visible_on_front' => 1
                        )
                      )
                    );
            } catch (Exception $e) {}*/

            // Required tables
            $statusTable = $installer->getTable('sales_order_grid');
            if($installer->getConnection()->tableColumnExists('sales_order_grid', 'trackingNumber') === false) 
            {
                $installer->getConnection()->addColumn(
                    $statusTable,
                    'trackingNumber',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' =>'Shipment tracking number.'
                    ]
                );
                $this->logger->info('LOBOT InstallSchema Success Add :: trackingNumber');
            }
            else
            {
                $this->logger->info('LOBOT InstallSchema Already have :: trackingNumber');
            }

            if($installer->getConnection()->tableColumnExists('sales_order_grid', 'shippingDate') === false) 
            {
                $installer->getConnection()->addColumn(
                    $statusTable,
                    'shippingDate',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' =>'Shipment Date.'
                    ]
                );
                $this->logger->info('LOBOT InstallSchema Success Add :: shippingDate');
            }
            else
            {
                $this->logger->info('LOBOT InstallSchema Already have :: shippingDate');
            }
            

            $installer->endSetup();
        } 
        catch (Exception $e) 
        {
            $this->logger->info('LOBOT InstallSchema Wrong :: ' . $e->__toString());
        }

       
    }

    public function installPreOrder(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $this->logger->info('TBA_PreOrder InstallSchema Start');
        /**
         * Prepare database for install
         */
        $installer->startSetup();

        try {
           $statusTable = $installer->getTable('sales_order_grid');
            if($installer->getConnection()->tableColumnExists('sales_order_grid', 'is_delivery_on_ready') === false) 
            {
                $installer->getConnection()->addColumn(
                    $statusTable,
                    'is_delivery_on_ready',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        'default' => false,
                        'nullable' => false,
                        'comment' =>'This order will send only on every item are ready when true.'
                    ]
                );
                $this->logger->info('TBA_PreOrder InstallSchema Success Add :: is_delivery_on_ready');
            }
            else
            {
                $this->logger->info('TBA_PreOrder InstallSchema Already have :: is_delivery_on_ready');
            }

        } catch (Exception $e) {}

        if(!$installer->tableExists('tba/preorde/order')) {

            $table = $installer->getConnection()->newTable($installer->getTable('tba_preorder_order'))
            ->addColumn(
                'pre_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false, 'identity' => true, 'primary' => true],
                'Preorder ID')
            ->addColumn(
                'pre_order_products_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false, 'primary' => true],
                'Preorder Products ID')
            ->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false],
                'Target Order')
            ->addColumn(
                'is_delivery_on_ready',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                 null,
                [ 'identity' => false, 'nullable' => false, 'default' => false],
                'Delivery on ready')
             ->addColumn(
                'is_saved',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                 null,
                [ 'identity' => false, 'nullable' => false, 'default' => false],
                'For save only one time when order status is Process')
            ->addColumn(
                'is_saved',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                 null,
                [ 'identity' => false, 'nullable' => false, 'default' => false],
                'For save only one time when order status is Process')
            ->addColumn(
                'create_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false],
                'Order Status');
            $installer->getConnection()->createTable($table);
        }

        if(!$installer->tableExists('tba/preorde/order/products')) {
            $table = $installer->getConnection()->newTable($installer->getTable('tba_preorder_order_products'))
            ->addColumn(
                'pre_order_products_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false, 'identity' => true, 'primary' => true],
                'Preorder Products ID')
            ->addColumn(
                'pre_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false, 'primary' => true],
                'Preorder ID')
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false],
                'Product ID')
            ->addColumn(
                'product_sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['unsigned' => false, 'nullable' => false],
                'Product Sku')
            ->addColumn(
                'remain_qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => false, 'nullable' => false],
                'Remain Qty');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}