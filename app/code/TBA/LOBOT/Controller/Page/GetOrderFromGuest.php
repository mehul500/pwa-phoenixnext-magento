<?php
namespace TBA\LOBOT\Controller\Page;

class GetOrderFromGuest extends \Magento\Framework\App\Action\Action
{
	protected $orderCollectionFactory;
	
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
	   \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory)
	{
	   $this->orderCollectionFactory = $orderCollectionFactory;
	   
	   parent::__construct($context);
	}
	
    public function execute()
    {
		$orderCollecion = $this->orderCollectionFactory
			->create()
			->addFieldToSelect('*');

		$orderCollecion
			->addAttributeToFilter('customer_is_guest', ['eq'=>1]);

		echo "<pre>";
		print_r($orderCollecion->getData());
		exit; 
		return $orderCollecion;
	} 
}

