<?php
namespace TBA\LOBOT\Controller\Page;

class GetOrderInWhile extends \Magento\Framework\App\Action\Action
{
	protected $orderCollectionFactory;
	protected $orderObject;
	protected $customerObject;
	
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
	   \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
	   \Magento\Sales\Model\Order $orderObject,
	   \Magento\Customer\Model\Customer $customerObject)
	{
	   $this->orderCollectionFactory = $orderCollectionFactory;
	   $this->orderObject = $orderObject;
	   $this->customerObject = $customerObject;
	   parent::__construct($context);
	}
	
    public function execute()
    {
		/*$dateNow = (new \DateTime())->format('Y-m-d H:i:s');
		echo  $days_ago = date('Y-m-d H:i:s', strtotime($dateNow))  . '<br>';
		
		$orderCollecion = $this->orderCollectionFactory
			->create()
			->addFieldToFilter('updated_at', ['lteq' => $days_ago]);

		echo $orderCollecion->getSelect()->__toString() . '<br>';
		*/
		//$orderList = $this->orderCollectionFactory->getList($orderCollecion);
		//print_r($orderCollecion->getData());
		$orderCollecion = $this->orderCollectionFactory
			->create()
			->addFieldToFilter('status', ['in' => 'pending_for_lobot']);

		echo $orderCollecion->getSelect()->__toString() . '<br>';
		
		foreach ($orderCollecion as $order) {
			$orderId = $order->getId();
			$customerId = $order->getCustomerId();
			
			echo 'Found order id :: ' . $orderId . '<br>';
			echo '--> Customer ID :: ' . $customerId . '<br>';
			echo '--> Customer Name :: ' . $order->getCustomerName() . '<br>';
			$shippingAddressObj = $order->getBillingAddress();
			if($shippingAddressObj)
			{
				$shippingAddressArray = $shippingAddressObj->getData();
				echo '--> Shipping Address' . '<br>';
				echo '  ------ firstname --> ' . $shippingAddressArray['firstname'] . '<br>';
				echo '  ------ lastname --> ' . $shippingAddressArray['lastname'] . '<br>';
				echo '  ------ postcode --> ' . $shippingAddressArray['postcode'] . '<br>';
				echo '  ------ city --> ' . $shippingAddressArray['city'] . '<br>';
				echo '  ------ region --> ' . $shippingAddressArray['region'] . '<br>';
				echo '  ------ street --> ' . $shippingAddressArray['street'] . '<br>';
				echo '  ------ telephone --> ' . $shippingAddressArray['telephone'] . '<br>';
			}
			$targetOrder = $this->orderObject->load($orderId);
			$orderItems = $targetOrder->getAllItems();
			echo '--> Found ' . count($orderItems) . ' Product from : ' . $orderId . '<br>';
			foreach ($orderItems as $product) {
				echo '  ---------- product ID --> ' . $product->getProductId() . '<br>';
				echo '  ---------- product Sku --> ' . $product->getSku() . '<br>';
				echo '  ---------- product Name --> ' . $product->getName() . '<br>';
			}
			echo '<br>';
			echo '=======================================================================================================';
			echo '<br>';
		}
		
		exit; 
		return $orderCollecion;
	} 
}