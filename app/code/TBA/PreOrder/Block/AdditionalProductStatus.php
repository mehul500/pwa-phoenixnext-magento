<?php
namespace TBA\PreOrder\Block;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Block\Cart\Additional\Info as AdditionalBlockInfo;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template as ViewTemplate;
use Magento\Framework\View\Element\Template\Context;
 
/**
 * AdditionalProInfo
 */
class AdditionalProductStatus extends \Magento\Framework\View\Element\Template
{
    protected $product = null;
    protected $productFactory;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
    */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ProductInterfaceFactory $productFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productFactory = $productFactory;
    }
 
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }
 
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }
 
    /**
     * @return additional information data
     */
    public function getAdditionalData()
    {
        $tempProduct = $this->getProduct();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCol = $objectManager->create('Magento\Catalog\Model\Product');
        //$productCol->clear();
        $product = $productCol->load($tempProduct->getId());

        $label = $product->getData('stock_status');

        switch ($label) 
        {
            case 'Ready':
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
            case 'PreOrder':
                $label = "<font color='red'>สินค้าพรี-ออเดอร์</font>";
                break;
            default :
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
        }
        // Do your code here
        return $label;
    }

    public function getProduct(): ProductInterface
    {
        /*if ($this->product instanceof ProductInterface) {

            return $this->product;
        }*/

        try 
        {
            $layout = $this->getLayout();
        } 
        catch (LocalizedException $e) 
        {
            $this->product = $this->productFactory->create();

            return $this->product;
        }

        /** @var AdditionalBlockInfo $block */
        $block = $layout->getBlock('additional.product.info');

        if ($block instanceof AdditionalBlockInfo) {
            $item = $block->getItem();
            $this->product = $item->getProduct();
        }

        return $this->product;
    }
}