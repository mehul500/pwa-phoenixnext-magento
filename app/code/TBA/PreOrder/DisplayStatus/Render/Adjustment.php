<?php

namespace TBA\PreOrder\DisplayStatus\Render;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\Render\AbstractAdjustment;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use Magento\Catalog\Helper\Data as CatalogData;
use Magento\Catalog\Pricing\Price\CustomOptionPrice;
use Magento\Weee\Helper\Data as WeeData;

/*use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Helper\Output\Spend;
use \Mirasvit\Rewards\Helper\Output\Earn;
use Mirasvit\Rewards\Helper\Data;*/

/**
 * @method string getIdSuffix()
 * @method string getDisplayLabel()
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Adjustment extends AbstractAdjustment
{
    /**
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        /*Config                 $config,
        Earn                   $earnOutput,
        Spend                  $spendOutput,
        Data                   $rewardsDataHelper,*/
        CatalogData            $catalogData,
        WeeData                $weeData,
        Session                $customerSession,
        PriceCurrencyInterface $priceCurrency,
        Template\Context       $context,
        array                  $data = []
    ) {
        /*$this->config            = $config;
        $this->earnOutput        = $earnOutput;
        $this->spendOutput       = $spendOutput;
        $this->rewardsDataHelper = $rewardsDataHelper;*/
        $this->catalogData       = $catalogData;
        $this->weeData           = $weeData;
        $this->customerSession   = $customerSession;

        parent::__construct($context, $priceCurrency, $data);
    }

    protected function getProduct()
    {
        return $this->getSaleableItem();
    }

     /**
     * {@inheritdoc}
     */
    protected function apply()
    {
        return $this->toHtml();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdjustmentCode()
    {
        return "display_status";//TBA\PreOrder\DisplayStatus\Adjustment::ADJUSTMENT_CODE;
    }

    /*public function isShowPoints()
    {
        \Magento\Framework\Profiler::start(__METHOD__);
        if ($this->isBundle() && $this->isProductPage()) {
            global $i;
            if (!$i) {
                $i = 0;
            }
            $i ++;
            if ($i == 2) { //need to find better way to show block only once on the bundle product page.
                \Magento\Framework\Profiler::stop(__METHOD__);
                return false;
            }
        }

        if ($this->isProductPage()) {
            $isAllowToShow = $this->config->getDisplayOptionsIsShowPointsOnProductPage();
        } else {
            $isAllowToShow = $this->config->getDisplayOptionsIsShowPointsOnFrontend();
        }

        $f = $isAllowToShow && !$this->isOptionPrice() && $this->getPointsFormatted();
        \Magento\Framework\Profiler::stop(__METHOD__);
        return $f;
    }*/

    public function isOptionPrice()
    {
        return $this->getAmountRender()->getPrice()->getPriceCode() == CustomOptionPrice::PRICE_CODE;
    }

    public function getCurrentStatus()
    {
        $n = $this->getProduct()->getData('stock_status');
        return $n;
    }

    //    /**
    //     * @return int
    //     */
    //    public function getMaxPointsForConfigurableProduct()
    //    {
    //        if ($this->getSaleableItem()->getTypeId() != Configurable::TYPE_CODE) {
    //            return;
    //        }
    //        $max  = [0];
    //        $item = $this->getSaleableItem();
    //
    //        $products = $item->getTypeInstance()->getUsedProducts($item);
    //
    //        foreach ($products as $product) {
    //            $max[] = $this->earnOutput->getProductPoints(
    //                $product
    //            );
    //        }
    //        return max($max);
    //    }

    private function isBundle()
    {
        return $this->getSaleableItem()->getTypeId() == \Magento\Bundle\Model\Product\Type::TYPE_CODE;
    }

    public function getPreOrderFormatted()
    {
        //\Magento\Framework\Profiler::start(__METHOD__);

        /*$priceId = $this->getData('price_id');
        $priceType = $this->getAmountRender()->getPrice()->getPriceCode();
        if ($priceType == \Magento\Bundle\Pricing\Price\BundleOptionPrice::PRICE_CODE) {
            $priceType = 'final_price';
        }*/
        //$label = __("<br>".'Earn %1 %2', $this->getLabel(), $this->rewardsDataHelper->formatPoints($points));
        //if ($this->isConfigurable()) {
        //    $label = __('Earn up to %1 %2', $this->getLabel(), $this->rewardsDataHelper->formatPoints($points));
        //}
        $label = $this->getCurrentStatus();
        switch ($label) 
        {
            case 'Ready':
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
            case 'PreOrder':
                $label = "<font color='red'>สินค้าพรี-ออเดอร์</font>";
                break;
            default :
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
        }
        /*if ($this->getCurrentStatus()) 
        {
            $label = __("<br>".'สินค้าพรี-ออเดอร์ of %1', $this->getSaleableItem()->getTypeId());
        }*/

        //\Magento\Framework\Profiler::start(__METHOD__);

        return $label;
    }

    public function buildIdWithPrefix($prefix)
    {
        $priceId = $this->getPriceId();
        if (!$priceId) {
            $priceId = $this->getSaleableItem()->getId();
        }
        return $prefix . $priceId . $this->getIdSuffix();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $label = '';
        if (!$this->getAmountRender()) {
            return $label;
        }

        switch ($this->getAmountRender()->getTypeId()) {
            case \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE:
                $label = 'starting at';
                break;
            case \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE:
                $label = 'Up to';
                break;
        }

        return $label;
    }
    /*private function isProductPage()
    {
        return !$this->getData('list_category_page') ||
            $this->getData('zone') == \Magento\Framework\Pricing\Render::ZONE_ITEM_VIEW;
    }*/
}
