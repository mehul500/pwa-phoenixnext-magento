define(
    [
        'ko',
        'uiComponent'
    ],
    function(ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'TBA_PreOrder/is-delivery-on-ready'
            },

            isChecked : true
        });
    }
);
