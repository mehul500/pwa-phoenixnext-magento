<?php

namespace TBA\GenCsv\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Eav\Model\Config;
use \Psr\Log\LoggerInterface;
// use Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Api\AddressRepositoryInterface;
class MassCustomer extends \Magento\Customer\Controller\Adminhtml\Index\AbstractMassAction
{
    protected $customerRepository;
    protected $customerResourceRepository;
    protected $eavConfig;
    protected $logger;
    protected $_customerFactory;
    protected $_addressFactory;
    protected $addressRepository;
    protected $_regionFactory;
    protected $_countryFactory;

  public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository,
        Config $eavConfig,
        LoggerInterface $logger,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        \Magento\Directory\Model\RegionFactory $regionFactory,
         \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        parent::__construct($context, $filter, $collectionFactory);
        $this->customerRepository = $customerRepository;
        $this->eavConfig = $eavConfig;
        $this->logger = $logger;
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->addressRepository = $addressRepository;
        $this->_regionFactory = $regionFactory;
        $this->_countryFactory = $countryFactory;

    }

  protected function massAction(AbstractCollection $collection)
  {

    $loggerMessage = "Customer Csv START  ";
    $this->logger->info($loggerMessage);
    
    $loggerMessage = "collection Customer getItems count is :: " . count($collection->getItems());
    $this->logger->info($loggerMessage);

    $loggerMessage = " GenCsv ";
    $this->logger->info($loggerMessage);

    $heading = [__('EMAIL')
                ,__('Name')
                ,__('Address')
                ,__('Tax ID')]; // set heading csv

    $dir = '/var/www/html/tmpLobotInterface/tmpCsv/';
    $outputFile = $dir."customer_report". date('Ymd_His').".csv";
    $handle = fopen($outputFile, 'w');
    fputcsv($handle, $heading);
    
    $row = array();

    foreach ($collection->getItems()  as $customer) {
        $customerId = $customer->getEntityId();
        $addressCustomer ='';
        $customerEmail = $customer->getEmail();
        $customerName = $customer->getName();
        $customerTaxVat = $customer->getTaxvat();
        $customerList = $this->customerRepository->getById($customerId);
        $addresses = $customerList->getAddresses();
        $street1 = '';
        $street2 = '';
        $street3 = '';
        $city = '';
        $region = '';
        $postcode = '';
        $countryId = '';
        $addressCustomer = '';

        if(count($addresses > 0)){
            if(!empty($addresses[0]))
            {
              $targetAddress = $addresses[0];
              $street = $targetAddress->getStreet();

              $city = $targetAddress->getCity();
              $loggerMessage = "Customer Csv city  ".$city;
              $this->logger->info($loggerMessage);

              $region = $targetAddress->getRegion()->getRegion();
              $loggerMessage = "Customer Csv region  ".$region;
              $this->logger->info($loggerMessage);

              $countryId = $targetAddress->getCountryId();
              $loggerMessage = "Customer Csv countryId  ".$countryId;
              $this->logger->info($loggerMessage);

              $postcode =$targetAddress->getPostcode();
              $loggerMessage = "Customer Csv postcode  ".$postcode;
              $this->logger->info($loggerMessage);

              $telephone = $targetAddress->getTelephone();
              $loggerMessage = "Customer Csv telephone  ".$telephone;
              $this->logger->info($loggerMessage);

              $company = $targetAddress->getCompany();
              $loggerMessage = "Customer Csv company  ".$company;
              $this->logger->info($loggerMessage);

              if(!empty($street[0])) {
                $street1 = $street[0];
              }     
              if(!empty($street[1])) {
                $street2 = $street[1];
              }   
              if(!empty($street[2])) {
                $street3 = $street[2];
              }  
            }
            $addressCustomer = $street1 ." ".$street2." ".$street3." ".$city." ".$region." ".$postcode ." ".$countryId;
            $loggerMessage = "Customer Csv addressCustomer  ".$addressCustomer;
            $this->logger->info($loggerMessage);
        }

        $loggerMessage = "Customer Csv customerId  ".$customerId;
        $this->logger->info($loggerMessage);
        $loggerMessage = "Customer Csv customerEmail  ".$customerEmail;
        $this->logger->info($loggerMessage);
        $loggerMessage = "Customer Csv customerName  ".$customerName;
        $this->logger->info($loggerMessage);
        $loggerMessage = "Customer Csv customerTaxVat  ".$customerTaxVat;
        $this->logger->info($loggerMessage);
        $loggerMessage = "===============" ;
        $this->logger->info($loggerMessage);

        // Set Array
        array_push($row,$customerEmail
                  ,$customerName
                  ,$addressCustomer
                  ,$customerTaxVat);
        fputcsv($handle, $row); // put to file csv
        unset($row); 
        $row = array();
    }
    $this->DownloadCsv($outputFile);
  }

  public function DownloadCsv($file){
    if (file_exists($file)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.basename($file).'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      readfile($file);
      exit;
    }
    unlink($file);
  }
}
